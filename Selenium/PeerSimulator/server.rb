require 'sinatra'
require_relative 'app/workers/peer_simulator_worker'

set :server, 'thin'

['/', '/start'].each do |path|
  post path do
    request.body.rewind
    data = JSON.parse(request.body.read)

    ramp_up_time    = data['ramp_up_time'].to_f
    amount_of_peers = data['amount_of_peers'].to_i
    seeder_time     = data['seeder_time'].to_i
    PeerSimulatorWorker.perform_async('start', amount_of_peers, ramp_up_time, seeder_time)
  end
end

post '/kill' do
  PeerSimulatorWorker.perform_async('kill')
end

