require 'selenium-webdriver'
require 'headless'
require 'sidekiq/logging'

module StreamIO
  class PeerSimulator
    TARGET_URL     = 'http://stream-io.herokuapp.com/?front=false'
    AMOUNT_OF_TABS = 10
    RAMP_UP_TIME   = 5 # Seconds
    VIDEO_DURATION = 900 # Seconds

    class << self
      def start!(amount_of_tabs, ramp_up_time, seeder_time)
        amount_of_tabs = amount_of_tabs > 0 ? amount_of_tabs : AMOUNT_OF_TABS
        ramp_up_time = ramp_up_time > 0 ? ramp_up_time : RAMP_UP_TIME
        execute!(amount_of_tabs, ramp_up_time, seeder_time)
      end

      def kill!
        logger.info "PeerSimulator - Going to kill #{drivers.size} drivers"
        drivers.each do |driver|
          logger.info "Killing #{driver}"
          driver.quit
        end
        @@drivers = []
      end

      def logger
        @@logger ||= Sidekiq::Logging.logger
      end

      def drivers
        @@drivers ||= []
      end

      def add_driver(driver)
        @@drivers = (drivers << driver)
      end

      private

      def execute!(amount_of_tabs, ramp_up_time, seeder_time)
        logger.info "PeerSimulator - Preparing #{amount_of_tabs} peers to be simulated, once every #{ramp_up_time} seconds"
        Headless.ly do
          logger.info "PeerSimulator - Starting peer 1 of #{amount_of_tabs}"
          driver = Selenium::WebDriver.for(:chrome)
          add_driver(driver)
          driver.get(TARGET_URL)

          (2..amount_of_tabs).each do |i|
            sleep(ramp_up_time)
            logger.info "PeerSimulator - Starting peer #{i} of #{amount_of_tabs}"
            driver.execute_script(build_js)
            a = driver.find_element(id: 'clickToOpen')
            a.click
          end
          sleep_time = VIDEO_DURATION + (ramp_up_time * amount_of_tabs) + seeder_time
          logger.info "PeerSimulator - Finished! Going to sleep for #{sleep_time} secs (seeder_time is #{seeder_time} secs)"
          sleep(sleep_time)
          logger.info "PeerSimulator - Finished seeping!"
        end
      end

      # Returns JS code that creates a hidden link tag to be clicked
      # by the selenium script
      def build_js
        "var a = document.createElement('a');
         var linkText = document.createTextNode('creates new peer');
         a.appendChild(linkText);
         a.href = '#{TARGET_URL}';
         a.target = '_blank';
         a.id = 'clickToOpen';
         a.setAttribute('style', 'color: white');
         var firstElement = document.body.childNodes[1];
         firstElement.insertBefore(a, firstElement.childNodes[1]);"
      end

    end
  end
end
