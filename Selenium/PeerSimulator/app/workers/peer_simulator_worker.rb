require 'sidekiq'
require_relative '../models/peer_simulator'
require_relative '../../config/sidekiq'

class PeerSimulatorWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(action, amount_of_peers = nil, ramp_up_time = nil, seeder_time = nil)
    case action
    when 'start'
      StreamIO::PeerSimulator.start!(amount_of_peers, ramp_up_time, seeder_time)
    when 'kill'
      StreamIO::PeerSimulator.kill!
    else
      StreamIO::PeerSimulator.logger.info("PeerSimulatorWorker - ERROR: Could not understand action #{action}")
    end
  end

end
