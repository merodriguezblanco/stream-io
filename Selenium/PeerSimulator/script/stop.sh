echo 'Stopping Sinatra'
kill -9 $(cat sinatra.pid)
echo '' > sinatra.pid

echo 'Stopping Sidekiq'
kill -9 $(cat sidekiq.pid)
echo '' > sidekiq.pid

echo 'Stopping Selenium'
kill -9 $(cat selenium.pid)
echo '' > selenium.pid
