#!/bin/sh
### BEGIN INIT INFO
# Provides:          StreamIO Gateway
# Required-Start:    $local_fs $network $named $time $syslog
# Required-Stop:     $local_fs $network $named $time $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Description:       Routes request to PeerSimulators
### END INIT INFO

start() {
  su -l mooveit -c "cd /home/mooveit/Proyecto-De-Grado/Selenium/PeerSimulator/script/; sh start.sh"
}

stop() {
  su -l mooveit -c "cd /home/mooveit/Proyecto-De-Grado/Selenium/PeerSimulator/script/; sh stop.sh"
}

case "$1" in
  start)
    start
    ;;
  stop)
    stop
    ;;
  retart)
    stop
    start
    ;;
  *)
    echo "Usage: $0 {start|stop|restart}"
esac
