echo 'Stopping Sinatra'
kill -9 $(cat sinatra.pid)
echo '' > sinatra.pid

echo 'Stopping Sidekiq'
kill -9 $(cat sidekiq.pid)
echo '' > sidekiq.pid

#echo 'Stopping Redis'
#kill -9 $(cat redis.pid)
#echo '' > redis.pid

echo 'Stopping Selenium'
kill -9 $(cat selenium.pid)
echo '' > selenium.pid
