# https://gist.github.com/trongthanh/1196596
# Setup the rate control and delay
sudo tc qdisc add dev lo root handle 1: htb default 12
sudo tc class add dev lo parent 1:1 classid 1:12 htb rate 128kbps
sudo tc qdisc add dev lo parent 1:12 netem delay 100ms

# Result:
# mooveit@192.168.1.39:~$ iperf -c localhost
# ------------------------------------------------------------
# Client connecting to localhost, TCP port 5001
# TCP window size:  648 KByte (default)
# ------------------------------------------------------------
# [  3] local 127.0.0.1 port 48668 connected with 127.0.0.1 port 5001
# [ ID] Interval       Transfer     Bandwidth
# [  3]  0.0-10.7 sec  2.12 MBytes  1.66 Mbits/sec

sudo tc qdisc add dev eth0 root handle 1: htb default 12
sudo tc class add dev eth0 parent 1:1 classid 1:12 htb rate 128kbps
sudo tc qdisc add dev eth0 parent 1:12 netem delay 100ms

# Result:
# mooveit@192.168.1.39$:~$ iperf -c 192.168.1.38
# ------------------------------------------------------------
# Client connecting to 192.168.1.38, TCP port 5001
# TCP window size: 22.9 KByte (default)
# ------------------------------------------------------------
# [  3] local 192.168.1.39 port 40746 connected with 192.168.1.38 port 5001
# [ ID] Interval       Transfer     Bandwidth
# [  3]  0.0-11.4 sec  1.75 MBytes  1.29 Mbits/sec

