echo 'Starting Selenium'
nohup java -jar bin/selenium-server-standalone-2.40.0.jar >> ../logs/selenium.log &
echo $! > selenium.pid

#echo 'Starting Redis'
#nohup redis-server >> ../logs/redis.log &
#echo $! > redis.pid

echo 'Starting Sidekiq'
nohup bundle exec sidekiq -r ../app/workers/peer_simulator_worker.rb >> ../logs/sidekiq.log &
echo $! > sidekiq.pid

echo 'Starting Sinatra'
nohup bundle exec ruby ../server.rb -o 0.0.0.0 >> ../logs/sinatra.log &
echo $! > sinatra.pid
