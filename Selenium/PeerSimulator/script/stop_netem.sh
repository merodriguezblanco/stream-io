#Remove the rate control/delay
sudo tc qdisc del dev lo root

# Result:
# mooveit@192.168.1.39:~$ iperf -c localhost
# ------------------------------------------------------------
# Client connecting to localhost, TCP port 5001
# TCP window size:  648 KByte (default)
# ------------------------------------------------------------
# [  3] local 127.0.0.1 port 48670 connected with 127.0.0.1 port 5001
# [ ID] Interval       Transfer     Bandwidth
# [  3]  0.0-10.0 sec  37.4 GBytes  32.2 Gbits/sec

sudo tc qdisc del dev eth0 root

# Result:
# mooveit@192.168.1.39:~$ iperf -c 192.168.1.38
# ------------------------------------------------------------
# Client connecting to 192.168.1.38, TCP port 5001
# TCP window size: 50.3 KByte (default)
# ------------------------------------------------------------
# [  3] local 192.168.1.39 port 40751 connected with 192.168.1.38 port 5001
# [ ID] Interval       Transfer     Bandwidth
# [  3]  0.0-10.0 sec  3.40 GBytes  2.92 Gbits/sec

