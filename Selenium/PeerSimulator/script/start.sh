echo 'Starting Selenium'
nohup java -jar bin/selenium-server-standalone-2.40.0.jar >> ../logs/selenium.log &
echo $! > selenium.pid

echo 'Starting Sidekiq'
nohup /home/mooveit/.rvm/gems/ruby-1.9.3-p545@selenium/wrappers/sidekiq -r ../app/workers/peer_simulator_worker.rb >> ../logs/sidekiq.log &
echo $! > sidekiq.pid

echo 'Starting Sinatra'
nohup /home/mooveit/.rvm/gems/ruby-1.9.3-p545@selenium/wrappers/ruby ../server.rb -o 0.0.0.0 >> ../logs/sinatra.log &
echo $! > sinatra.pid
