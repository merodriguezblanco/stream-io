require 'sinatra'
require 'json'
require_relative 'app/models/router'
require_relative 'app/models/auth'

set :server, 'thin'
set :port, 7878

post '/' do
  request.body.rewind
  data = JSON.parse(request.body.read)

  if StreamIO::Auth.authorize?(data['token'])
    router = StreamIO::Router.instance(10)
    router.forward(data['vm_ids'], data['messages'], data['action'], data['ramp_up'].to_i, data['fw_to_gw'])
  else
    halt 401, "No no no no, you are not authorized...\n"
  end
end

