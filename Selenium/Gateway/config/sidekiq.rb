require 'sidekiq'
require 'uuid'

class SidekiqConfig
  class << self
    CONFIG_FILE = 'SIDEKIQ_NAMESPACE'

    def uuid
      if exists?
        fetch
      else
        uuid = Time.now.to_f.to_s.split('.').join
        uuid += UUID.new.generate
        store(uuid)
      end
    end

    def store(uuid)
      File.open(config_file_dir, 'w') do |f|
        f.write(uuid)
      end
      uuid
    end

    def fetch
      File.open(config_file_dir, 'r') do |f|
        f.read
      end
    end

    def exists?
      File.exists?(config_file_dir)
    end

    def config_file_dir
      File.join(File.dirname(__FILE__), CONFIG_FILE)
    end
  end
end

REDIS_SERVER = 'redis://stream-io-redis:123redis@pub-redis-13931.us-east-1-4.2.ec2.garantiadata.com:13931/0'

Sidekiq.configure_server do |config|
  config.redis = { url: REDIS_SERVER, namespace: SidekiqConfig.uuid }
end

Sidekiq.configure_client do |config|
  config.redis = { url: REDIS_SERVER, namespace: SidekiqConfig.uuid }
end

