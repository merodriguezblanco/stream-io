require 'sidekiq'
require_relative '../models/router'
require_relative '../../config/sidekiq'

class GwWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(vm_ips, messages, action, ramp_up, fw_to_gw)
    router = StreamIO::Router.instance

    if vm_ips.size == 1 && messages.size == 1
      router.forward_1_to_1(messages.first, vm_ips.first, action, ramp_up)
    elsif vm_ips.size > 1 && messages.size > 1
      router.forward_many_to_many(messages, vm_ips, action, ramp_up)
    elsif vm_ips.size > 1 && messages.size == 1
      router.forward_1_to_many(messages.first, vm_ips, action, ramp_up)
    else
      raise "Unsupported message/vm_ip format"
    end

    router.forward_to_gw(fw_to_gw['url'], fw_to_gw['payload']) if fw_to_gw
  end


  def self.logger
    @@logger ||= Sidekiq::Logging.logger
  end

end
