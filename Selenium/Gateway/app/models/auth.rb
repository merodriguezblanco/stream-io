module StreamIO
  class Auth
    class << self
      CONFIG_FILE = 'config/SECRET_TOKEN'

      def authorize?(token)
        secret_token == token
      end

      private

      def secret_token
        @@secret_token ||= load_secret_token
      end

      def load_secret_token
        file = File.join(File.dirname(__FILE__), '/../..', CONFIG_FILE)
        token = nil
        File.open(file, 'r') do |f|
          f.each_line do |line|
            token = line.split("\n").first
          end
        end
        token
      end
    end
  end
end
