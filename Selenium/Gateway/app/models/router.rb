require 'rest-client'
require_relative '../workers/gw_worker'

module StreamIO
  LAN                 = '192.168.1'
  DEFAULT_REMOTE_PORT = 4567

  VMS = {
    1  => "#{LAN}.201",
    2  => "#{LAN}.202",
    3  => "#{LAN}.203",
    4  => "#{LAN}.204",
    5  => "#{LAN}.205",
    6  => "#{LAN}.206",
    7  => "#{LAN}.207",
    8  => "#{LAN}.208",
    9  => "#{LAN}.209",
    10 => "#{LAN}.210",
    11 => "#{LAN}.211",
    12 => "#{LAN}.212",
    13 => "#{LAN}.213",
    14 => "#{LAN}.214",
    15 => "#{LAN}.215",
    16 => "#{LAN}.216",
    17 => "#{LAN}.217",
    18 => "#{LAN}.218",
    19 => "#{LAN}.219",
    20 => "#{LAN}.220"
  }

  class Router
    attr_accessor :vms

    def initialize(amount_of_vms, first_ip_id = nil)
      if first_ip_id.nil?
        @vms = VMS
      else
        @vms = {}
        ip_id = first_ip_id
        (1..amount_of_vms).each do |vm_id|
          @vms[vm_id] = "#{LAN}.#{ip_id}"
          ip_id += 1
        end
      end
    end

    def self.instance(amount_of_vms = nil, first_ip_id = nil)
      @@instace ||= new(amount_of_vms, first_ip_id)
    end

    # @vm_ids [Array]
    # @messages [Array of JSON]
    #
    # Takes @vm_ids and @messages and forwards the @messages to the
    # corresponding VM.
    def forward(vm_ids, messages, action, ramp_up, fw_to_gw)
      vm_ips = route_many(vm_ids)
      GwWorker.perform_async(vm_ips, messages, action, ramp_up, fw_to_gw)
    end

    def forward_many_to_many(messages, vm_ips, action, ramp_up)
      vm_ips.each do |vm_ip|
        forward_1_to_1(messages.shift, vm_ip, action, ramp_up)
      end
    end

    def forward_1_to_many(message, vm_ips, action, ramp_up)
      vm_ips.each do |vm_ip|
        forward_1_to_1(message, vm_ip, action, ramp_up)
      end
    end

    def forward_1_to_1(message, vm_ip, action, ramp_up)
      GwWorker.logger.info "Delaying #{ramp_up} ramp_up seconds"
      sleep(ramp_up)
      GwWorker.logger.info "Posting to #{vm_ip}:#{DEFAULT_REMOTE_PORT}/#{action} message: #{message.to_json}"
      RestClient.post("#{vm_ip}:#{DEFAULT_REMOTE_PORT}/#{action}", message.to_json, content_type: :json, accept: :json)
    end

    def forward_to_gw(url, payload)
      GwWorker.logger.info "Forwarding message #{payload.to_json} to Gateway #{url}"
      RestClient.post(url, payload.to_json, content_type: :json, accept: :json)
    end

    def route(vm_id)
      @vms[vm_id]
    end

    def route_many(vm_ids)
      vm_ids.map { |vm_id| route(vm_id) }
    end
  end
end

