echo 'Starting Sidekiq'
nohup /home/mooveit/.rvm/gems/ruby-1.9.3-p545@router/wrappers/sidekiq -r ../app/workers/gw_worker.rb >> ../logs/sidekiq.log &
echo $! > sidekiq.pid

echo 'Starting Sinatra'
nohup /home/mooveit/.rvm/gems/ruby-1.9.3-p545@router/wrappers/ruby ../server.rb -o 0.0.0.0 >> ../logs/sinatra.log &
echo $! > sinatra.pid
