echo 'Starting Sinatra'
nohup bundle exec ruby ../server.rb -o 0.0.0.0 >> ../logs/sinatra.log &
echo $! > sinatra.pid

echo 'Starting Sidekiq'
nohup bundle exec sidekiq -r ../app/workers/gw_worker.rb >> ../logs/sidekiq.log &
echo $! > sidekiq.pid

