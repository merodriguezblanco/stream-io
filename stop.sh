#!/bin/bash

# Stop ApplicationServers

cd ApplicationServer
echo 'Stopping ApplicationServer'
kill -9 $(cat server.pid)
echo '' > server.pid

# Stop Tracker

#cd ../Tracker
#echo 'Stopping Tracker'
#kill -9 $(cat server.pid)
#echo '' > server.pid

# Stop PeerJS

cd ../Selenium
echo 'Stopping Selenium'
kill -9 $(cat server.pid)
echo '' > server.pid

echo 'Done!'

