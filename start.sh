#!/bin/bash

# Start ApplicationServers

cd ApplicationServer
echo 'Starting ApplicationServer'
nohup node server.js >> server.log &
echo $! > server.pid

# Start Tracker

#cd ../Tracker
#echo 'Starting Tracker'
#nohup node server.js >> server.log &
#echo $! > server.pid

echo 'Done!'

