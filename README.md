#Stream-IO

Stream-IO is a combination of components that work together to provide P2P multimedia streaming between web browsers. It uses basic entities from the BitTorrent protocol like peers, swarm, trackers and CDNs. There is also an application server that serves a web page that as a client reproduces the video.

##Use case

A user access a web page that reproduces a video composed of chunks provided either by CDNs or by other web browsers via P2P, depending on the swarm status and a set of fair-trade algorithms.
There is also a tracker storing statistics that allow to monitor the state of the swarm, savings and performance metrics. This allows to analyze how the system behaves and come to conclusions based on that.

##Involved technologies
- HTML 5
- WebRTC
- WebSockets
- Media Source API
- Javascript
- Require JS
- Other JS Libraries

##Future Work
This prototype could be extended to support sharing of static components of any kind. That way, web pages that use Stream-IO would end up improving UX by increasing downloading performance and reducing CDN usage and hence, costs.

##Getting Started

After the repo is cloned and user is standing on the cloned dir.
```
git submodule init
git submodule update
cd ApplicationServer
npm install
node server.js
```
ApplicationServer will run in port 5000.

Please note that this will make the ApplicationServer run with the Tracker, PeerJS and CDNs running on the cloud.
All other components need to be running locally otherwise, and configs need to be changed in the ApplicationServer to point to them.

##Deployment additional information

To install init.d scripts in server:

Copy project_name/script/init.d into /etc/init.d/script_name

```
sudo chmod 755 script_name
sudo update-rc.d script_name defaults
sudo update-rc.d script_name enable
```
As the start.sh scripts shows, some commands need to run inside a RVM
environment, with the proper gemset and gems.

To do so from a script, the command needs to be wrapped first.
```
cd project_folder
rvm wrapper ruby_version@gemset command
```
